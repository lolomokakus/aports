# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-succulent
pkgver=0.2.7
pkgrel=0
pkgdesc="Collect POST requests easily"
url="https://github.com/firefly-cpp/succulent"
arch="noarch"
license="MIT"
depends="python3 py3-flask py3-pandas py3-yaml"
makedepends="py3-gpep517 py3-poetry-core"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/firefly-cpp/succulent/archive/$pkgver/succulent-$pkgver.tar.gz"
builddir="$srcdir/succulent-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest -k 'not TestProcessing'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/succulent-$pkgver-py3-none-any.whl
}

sha512sums="
286ec37e381167e346d37a9da43a91a3d06a424f0310c6252d939cffc8894b4ea02de6d65ce559084afeea8238a865f0a8a02572eda2ffdd28fd51bec3ec7e2d  succulent-0.2.7.tar.gz
"
