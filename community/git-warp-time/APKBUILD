# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=git-warp-time
pkgver=0.7.0
pkgrel=0
pkgdesc="Reset timestamps of Git repository files to the time of the last modifying commit"
url="https://github.com/alerque/git-warp-time"
license="GPL-3.0-only"
arch="all"
makedepends="cargo cargo-auditable"
subpackages="$pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"
options="net"
source="https://github.com/alerque/git-warp-time/archive/v$pkgver/git-warp-time-$pkgver.tar.gz"

prepare() {
	default_prepare

	# Doc-tests from README.md expect to be ran in a Git repository
	git init -q .
	git config --local user.name "example"
	git config --local user.email "example@example.com"
	git add .
	git commit -q -m "init"

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/

	# shell completions
	find target/release -name $pkgname.bash \
		-exec install -Dm644 {} "$pkgdir"/usr/share/bash-completion/completions/$pkgname \;
	find target/release -name $pkgname.fish \
		-exec install -Dm644 {} "$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish \;
	find target/release -name _$pkgname \
		-exec install -Dm644 {} "$pkgdir"/usr/share/zsh/site-functions/_$pkgname \;
}

sha512sums="
1868ba1221a54a8032b94f797acab1e1f650e7786302417121765542402373ab6855af8b0024aa7ba4926fcc0e51902a232570c18f8a948c2548ba165d8cb281  git-warp-time-0.7.0.tar.gz
"
